resource "aws_key_pair" "deployer" {
  key_name = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKxzVXVH5ghiP8nu8p8fBaJId1/Q2pku4YmVa9HGpIT8iTMHV1j9isAVrYy/Cs+mmoMscDYkYvNNA1GMYt2KiDXQt9IaNNeLRiDFjZDmtEuj3oCeQE9D3tXHFbrtSZcqseImjHbN2ilJLXTL+mZKegRB9pQZm1ZfCF5/WcgktFD796JK/tdnZrb7U8zK2XRtE56C8A1oYiueAkGrUZv3o+BYiOBUklvfLveCyrd76YqZeqq5oBrcOVHVGn05c71/xcsTVDfiDOHwbPd2Kkhsn9xs9ghF+CDAxpOgrx18vn9f0oex6mWv7m1b+OUGPfHbA1I0XrIyZQaXI+u1nwMTHeXYABoN/+qGzoYfsJIzrA28/0arkS1ScSSon9TCfViGJY5Z9JdywnXR30zoa/RHZIe0cLxa29rcdXqFTNbI8neSNIEhtaLg6zG+5bA71phUkUaG+fWkyytyqAWL0bQWVmkDH0oR6rcN4mCx3Yn6eY60qfwJy5U8nNTe9j+5rqEiU= theodore@ubuntu"

}

resource "aws_default_vpc" "vpc" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "security_group" {
  vpc_id = aws_default_vpc.vpc.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = "HelloWorld"
  }

}

output "output_IP" {
  value = aws_instance.web.public_ip
}
